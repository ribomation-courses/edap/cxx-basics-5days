#include <iostream>
#include <iomanip>
#include <string>

using namespace std;
using namespace std::string_literals;

template<typename T>
bool equals(T a, T b, T c) {
    return (a == b) && (b == c);
}

int main() {
    cout << boolalpha
         << "equals<int>: "
         << equals<int>(21, 3 * 7, 2 * 10 + 1)
         << endl;
    cout << boolalpha
         << "equals<float>: "
         << equals<float>(21.0, 3 * 7.0F, 2 * 10 + 1.0F)
         << endl;
    cout << boolalpha
         << "equals<string>: "
         << equals<string>("hello"s, "he"s + "llo"s,
                           "hello world"s.substr(0,5))
         << endl;
    return 0;
}
