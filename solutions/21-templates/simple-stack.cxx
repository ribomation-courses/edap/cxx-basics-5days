#include <iostream>
#include <string>

using namespace std;

template<typename T=int, unsigned N = 12>
class Stack {
    T        stk[N];
    unsigned top = 0;
public:
    void push(T x) { stk[top++] = x; }
    T    pop()     { return stk[--top]; }
    bool empty() const { return top == 0; }
    bool full()  const { return top >= N; }
};

void stack_of_ints() {
    Stack<>   s;
    for (auto k = 1; !s.full(); ++k) s.push(k);
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}

void stack_of_string() {
    using namespace string_literals;
    Stack<string, 5> s;
    for (auto k = 1; !s.full(); ++k) s.push("str-"s + to_string(k));
    while (!s.empty()) cout << s.pop() << " ";
    cout << endl;
}

int main(int, char**) {
    stack_of_ints();
    stack_of_string();
    return 0;
}

