#include <iostream>

using namespace std;

using ArgumentType = unsigned short;
using ResultType = unsigned long;

template<ArgumentType N>
struct Fibonacci {
    constexpr static ArgumentType argument = N;
    constexpr static ResultType   result   = Fibonacci<N - 2>::result + Fibonacci<N - 1>::result;
};

template<>
struct Fibonacci<1> {
    constexpr static ArgumentType argument = 1;
    constexpr static ResultType   result   = 1;
};

template<>
struct Fibonacci<0> {
    constexpr static ArgumentType argument = 0;
    constexpr static ResultType   result   = 0;
};


int main(int, char**) {
    struct Result {
        ArgumentType argument;
        ResultType   result;
    };

    Result results[] = {
            {Fibonacci<1>::argument,  Fibonacci<1>::result},
            {Fibonacci<2>::argument,  Fibonacci<2>::result},
            {Fibonacci<3>::argument,  Fibonacci<3>::result},
            {Fibonacci<4>::argument,  Fibonacci<4>::result},
            {Fibonacci<5>::argument,  Fibonacci<5>::result},
            {Fibonacci<10>::argument, Fibonacci<10>::result},
            {Fibonacci<15>::argument, Fibonacci<15>::result},
            {Fibonacci<30>::argument, Fibonacci<30>::result},
            {Fibonacci<40>::argument, Fibonacci<40>::result},
            {Fibonacci<42>::argument, Fibonacci<42>::result},
            {Fibonacci<45>::argument, Fibonacci<45>::result},
            {Fibonacci<50>::argument, Fibonacci<50>::result},
            {Fibonacci<55>::argument, Fibonacci<55>::result},
            {Fibonacci<60>::argument, Fibonacci<60>::result},
    };

    for (auto r : results)
        cout << r.argument << ": " << r.result << endl;
    return 0;
}


