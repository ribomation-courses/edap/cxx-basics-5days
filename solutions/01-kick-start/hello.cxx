//compile: g++ -std=c++17 -Wall hello.cxx -o hello

#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, char** argv) {
    string  name = (argc==1) ? "Nisse" : argv[1];
    cout << "Hi " << name << ", welcome to C++" << endl;

    vector<string>  words = {"C++", "is", "really", "cool", "!!!"};
    for (auto& w : words) cout << w << ' ';
    cout << endl;
}

