#include <iostream>
#include <string>
#include <functional>

using namespace std;

void print(const string &name, int arr[], unsigned n);

double reduce(int *arr, const unsigned n, function<double(double, int)> aggregator);

int main() {
    int arr[] = {10, 2, 35, 42, 5};
    const unsigned size = sizeof(arr) / sizeof(arr[0]);
    print("arr", arr, size);

    auto sum = reduce(arr, size, [](auto acc, auto n) { return acc + n; });
    cout << "sum: " << sum << endl;

    auto prod = reduce(arr, size, [](auto acc, auto n) { return acc * n; });
    cout << "prod: " << prod << endl;

    auto max = reduce(arr, size, [](auto acc, auto n) {
        return n > acc ? n : acc;
    });
    cout << "max: " << max << endl;

    auto min = reduce(arr, size, [](auto acc, auto n) {
        return n < acc ? n : acc;
    });
    cout << "min: " << min << endl;

    return 0;
}

double reduce(int *arr, const unsigned n, function<double(double, int)> aggregator) {
    double result = arr[0];
    for (auto k = 1U; k < n; ++k) {
        result = aggregator(result, arr[k]);
    }
    return result;
}

void print(const string &name, int *arr, unsigned n) {
    cout << name << ": [" << arr[0];
    for (auto k = 1U; k < n; ++k)
        cout << ", " << arr[k];
    cout << "]\n";
}

