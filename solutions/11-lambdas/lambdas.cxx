#include <iostream>
#include <string>

using namespace std;
void print(const string &name, int arr[], unsigned n);


int main() {
    int arr[] = {1, 2, 3, 4, 5};
    const unsigned size = sizeof(arr) / sizeof(arr[0]);
    print("arr", arr, size);

    int N = 42;
    auto f = [&](){
        for (auto k=0U; k<size; ++k) {
            arr[k] += N;
        }
    };
    f();
    print("arr", arr, size);

    return 0;
}



void print(const string &name, int *arr, unsigned n) {
    cout << name << ": [" << arr[0];
    for (auto k = 1U; k < n; ++k)
        cout << ", " << arr[k];
    cout << "]\n";
}


