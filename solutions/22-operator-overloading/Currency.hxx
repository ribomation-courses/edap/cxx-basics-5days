#pragma once

#include <iostream>
#include <string>
#include <utility>

using namespace std;


class Currency {
    const string code;
    double xe   = 1;

public:
    Currency(const string& code, double rate) : code(code), xe(rate) { }

    Currency() = default;
    ~Currency() = default;
    Currency(const Currency&) = default;
    Currency& operator =(const Currency&) = delete;

    Currency& operator =(double rate) {
        Currency::xe = rate;
        return *this;
    }

    double rate() const {
        return xe;
    }

    string name() const {
        return code;
    }

    operator double() const {
        return rate();
    }

    operator string() const {
        return name();
    }

    friend ostream& operator <<(ostream& os, const Currency& c) {
        return os << c.name() << ":" << c.rate();
    }

};

inline bool operator ==(const Currency& left, const Currency& right) {
    return left.name() == right.name();
}

inline bool operator <(const Currency& left, const Currency& right) {
    return left.name() < right.name();
}


