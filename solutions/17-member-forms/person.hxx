#pragma once

#include <string>
#include <ostream>
#include "dog.hxx"

using namespace std;
using namespace std::string_literals;


class Person {
    string   name;
    unsigned age;
    Dog* dog;
public:
    Person(const string& name, unsigned int age = 42, Dog* dog = nullptr)
            : name(name), age(age), dog(dog) {}

    const string& getName() const {
        return name;
    }

    unsigned int getAge() const {
        return age;
    }

    Dog& getDog() const {
        return *dog;
    }

    void setDog(Dog* dog) {
        Person::dog = dog;
    }

    friend ostream& operator<<(ostream& os, const Person& person) {
        os << "name: " << person.getName()
           << ", age: " << person.getAge()
           << ", dog: " << (person.dog ? person.getDog().getName() : "NO dog"s);
        return os;
    }
};


