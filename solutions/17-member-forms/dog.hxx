#pragma  once
#include <iostream>
#include <string>
using namespace std;

class Dog {
    string name;
public:
    Dog(const string& name) : name(name) {
    }

    void bark() const {
        cout << name << " BARk BARK\n";
    }

    const string& getName() const {
        return name;
    }

    friend ostream& operator<<(ostream& os, const Dog& dog) {
        os << "name: " << dog.name;
        return os;
    }
};
