#include <iostream>
#include "dog.hxx"
#include "person.hxx"

int main() {
    Dog fido{"Fido"};
    Person nisse{"Nisse", 42, &fido};
    cout << "nisse: " << nisse << endl;

    Person anna{"Anna"};
    cout << "anna: " << anna << endl;

    anna.setDog(&fido);
    cout << "anna: " << anna << endl;

    nisse.setDog(nullptr);
    cout << "nisse: " << nisse << endl;

    return 0;
}

