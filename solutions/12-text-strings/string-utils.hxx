#pragma once

#include <string>
#include <cctype>

namespace StringUtils {
    using namespace std;

    inline string toLowerCase(string s) {
        for (auto k = 0U; k < s.size(); ++k) {
            s[k] = static_cast<char>(tolower(s[k]));
        }
        return s;
    }

    inline string toUpperCase(string s) {
        for (auto k = 0U; k < s.size(); ++k) {
            s[k] = static_cast<char>(toupper(s[k]));
        }
        return s;
    }

    inline string capitalize(string s) {
        return toUpperCase(s.substr(0, 1)) + toLowerCase(s.substr(1));
    }

    inline string strip(string s) {
        string result;
        for (auto ch : s) if (isalpha(ch)) result += ch;
        return result;
    }

    inline string truncate(string s, const unsigned width) {
        if (s.size() <= width) return s;
        return s.substr(0, width);
    }

}
