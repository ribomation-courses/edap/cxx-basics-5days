#include <iostream>
#include <cassert>
#include "string-utils.hxx"

using namespace std;
using namespace std::string_literals;
using namespace StringUtils;


int main() {
    assert(toLowerCase("Tjolla Hopp 123 ABC"s) == "tjolla hopp 123 abc"s);
    assert(toUpperCase("Tjolla Hopp 123 ABC"s) == "TJOLLA HOPP 123 ABC"s);
    assert(capitalize("hello"s) == "Hello"s);
    assert(capitalize("HELLO"s) == "Hello"s);
    assert(strip("Tjolla Hopp 123 ABC"s) == "TjollaHoppABC"s);
    assert(truncate("Tjolla Hopp 123 ABC"s, 6) == "Tjolla"s);
    assert(truncate("ABC"s, 6) == "ABC"s);
    cout << "All tests passed\n";
    return 0;
}
