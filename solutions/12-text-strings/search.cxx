#include <iostream>
#include <string>
using namespace std;

int main(int argc, char **argv) {
    if (argc <= 1) {
        cerr << "No search phrase\n";
        cerr << "usage: " << argv[0] << " {phrase} < input\n";
        return 1;
    }

    const string phrase = argv[1];
    unsigned lineno = 1;
    for (string line; getline(cin, line); ++lineno) {
        if (line.find(phrase) != string::npos) {
            cout << lineno << ") " << line << endl;
        }
    }

    return 0;
}
