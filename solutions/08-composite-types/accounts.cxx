#include <iostream>
#include <string>

using namespace std;

enum class AccountType {
    savings = 10, checking = 20, transfer = 30
};

string toString(AccountType t) {
    switch (t) {
        case AccountType::checking:
            return "checking";
        case AccountType::savings:
            return "savings";
        case AccountType::transfer:
            return "transfer";
    }
}

struct Account {
    AccountType type;
    int balance;
    float rate;
};

string toString(Account acc) {
    string result = "";
    result += "Account{" + toString(acc.type);
    result += ", SEK " + to_string(acc.balance);
    result += ", " + to_string(acc.rate) + "%";
    result += "}";
    return result;
}


int main() {
    Account acc1 = {AccountType::savings, 100, 1.5};
    cout << "acc1: " << toString(acc1) << endl;

    Account acc2 = {AccountType::transfer, -500, 0.75};
    cout << "acc2: " << toString(acc2) << endl;

    return 0;
}
