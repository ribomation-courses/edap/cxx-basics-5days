#include <iostream>

using namespace std;

int main() {
    int value = 42;
    cout << "value = " << value << endl;

    int* ptr = &value;
    cout << "ptr = " << ptr << endl;
    cout << "*ptr = " << *ptr << endl;

    *ptr *= 10;
    cout << "value = " << value << endl;
    cout << "*ptr = " << *ptr << endl;

    return 0;
}
