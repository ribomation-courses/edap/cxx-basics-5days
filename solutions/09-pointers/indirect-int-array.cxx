#include <iostream>

using namespace std;

int main() {
    int arr[] = {1, 2, 3, 4, 5};
    const auto N = sizeof(arr) / sizeof(arr[0]);
    cout << "N=" << N << endl;

    for (auto k = 0U; k < N; ++k) {
        cout << "arr[" << k << "] = " << arr[k] << endl;
    }

    int *ptr = &(arr[0]);
    cout << "*ptr = " << *ptr << endl;
    cout << "*ptr = " << *(&(arr[0])) << endl;
    cout << "*ptr = " << *arr << endl;

    for (int *q = arr; q < &arr[N]; ++q) {
        cout << "*q = " << *q << endl;
    }

    for (int *q = arr; q < (arr + N); ++q) {
        *q *= 10;
    }
    for (int *q = arr; q < &arr[N]; ++q) {
        cout << "*q = " << *q << endl;
    }
    for (auto k = 0U; k < N; ++k) {
        cout << "arr[" << k << "] = " << arr[k] << endl;
    }

    return 0;
}
