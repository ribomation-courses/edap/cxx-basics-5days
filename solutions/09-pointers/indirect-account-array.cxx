#include <iostream>

using namespace std;

struct Account {
    int balance;
    double rate;
};

int main() {
    Account arr[] = {
            {100, 1.0},
            {200, 1.25},
            {300, 1.5},
            {400, 1.75},
            {500, 2.0},
    };
    const auto N = sizeof(arr) / sizeof(arr[0]);
    cout << "N=" << N << endl;

    for (Account *q = arr; q < (arr + N); ++q) {
        cout << "*ptr = SEK " << q->balance << ", " << q->rate << "%\n";
    }

    cout << "---------------\n";
    for (Account *q = arr; q < (arr + N); ++q) {
        q->balance *= 1 + q->rate / 100;
    }
    for (Account *q = arr; q < (arr + N); ++q) {
        cout << "*ptr = SEK " << q->balance << ", " << q->rate << "%\n";
    }

    return 0;
}
