cmake_minimum_required(VERSION 3.10)
project(09_pointers)

set(CMAKE_CXX_STANDARD 17)

add_executable(indirect-int indirect-int.cxx)
add_executable(indirect-int-array indirect-int-array.cxx)
add_executable(indirect-account indirect-account.cxx)
add_executable(indirect-account-array indirect-account-array.cxx)
