#include <iostream>

using namespace std;

struct Account {
    int balance;
    double rate;
};

int dummy() {return 42;}

int main() {
    cout << "sizeof(Account) = " << sizeof(Account) << endl;

    Account acc = {1000, 1.25};
    cout << "acc: SEK " << acc.balance << ", " << acc.rate << "%\n";

    Account *ptr = &acc;
    cout << "ptr = " << ptr << endl;
    cout << "*ptr = SEK " << (*ptr).balance << ", " << ptr->rate << "%\n";

    ptr->balance *= 10;
    cout << "*ptr = SEK " << (*ptr).balance << ", " << ptr->rate << "%\n";

    cout << "ptr = " << ptr << endl;
    cout << "&ptr.balance = " << &(ptr->balance) << endl;
    cout << "&ptr.rate    = " << &(ptr->rate) << endl;

    return 0;
}
