#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
#include <cctype>
#include <utility>
#include <numeric>
#include <random>
#include <iomanip>

using namespace std;
using namespace std::string_literals;

using Frequencies = unordered_map<string, unsigned>;
using WordFreq = pair<string, unsigned>;

Frequencies load(istream&, unsigned);
vector<string> extractWords(string, unsigned);
vector<WordFreq> mostFrequent(Frequencies, unsigned);
vector<string> toTags(vector<WordFreq> words);
string join(vector<string> strings, string delim);
string randColor();

const string htmlPrefix = "<html><body>\n";
const string htmlSuffix = "</body></html>\n";


int main(int argc, char** argv) {
    unsigned     maxWords = 100;
    unsigned int minSize  = 4;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-w")
            maxWords = static_cast<unsigned>(stoi(argv[++k]));
        else if (arg == "-s")
            minSize = static_cast<unsigned int>(stoi(argv[++k]));
    }

    auto freqs = load(cin, minSize);
    auto words = mostFrequent(freqs, maxWords);

    auto tags = toTags(words);
    random_shuffle(tags.begin(), tags.end());
    string htmlTags = join(tags, "\n");
    cout << htmlPrefix << htmlTags << htmlSuffix << endl;

    return 0;
}

string join(vector<string> strings, string delim) {
    return accumulate(strings.begin(), strings.end(), ""s, [=](auto acc, auto str) {
        return acc + delim + str;
    });
}

vector<string> toTags(vector<WordFreq> words) {
    auto   maxFontSize = 120.0;
    auto   minFontSize = 30.0;
    auto   maxFreq     = words.front().second;
    auto   minFreq     = words.back().second;
    double scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    vector<string> tags;
    for (auto[word, freq] : words) {
        unsigned fontSize = freq * scale;
        string   color    = randColor();

        ostringstream buf;
        buf << "<span style='font-size:" << fontSize
            << "px; color:#" << color << ";'>"
            << word << "</span>";
        tags.push_back(buf.str());
    }

    return tags;
}

string randColor() {
    static random_device               r;
    uniform_int_distribution<unsigned> color{0, 255};
    ostringstream                      buf;
    buf << hex << color(r) << color(r) << color(r);
    return buf.str();
}

Frequencies load(istream& in, unsigned minSize) {
    Frequencies freqs;
    for (string line; getline(in, line);) {
        vector<string> words = extractWords(line, minSize);
        for (auto& w : words) {
            freqs[w]++;
        }
    }
    return freqs;
}

vector<string> extractWords(string line, unsigned minSize) {
    transform(line.begin(), line.end(), line.begin(), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });

    vector<string> result;
    istringstream  buf{line};
    for (string    word; buf >> word;) {
        if (word.size() >= minSize) {
            result.push_back(word);
        }
    }

    return result;
};

vector<WordFreq> mostFrequent(Frequencies freqs, unsigned int N) {
    vector<WordFreq> sortable{freqs.begin(), freqs.end()};

    sort(sortable.begin(), sortable.end(), [](auto left, auto right) {
        return left.second > right.second;
    });

    vector<WordFreq> mostFrequent;
    copy_n(sortable.begin(), N, back_inserter(mostFrequent));
    return mostFrequent;
}


