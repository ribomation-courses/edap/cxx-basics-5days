#include <iostream>
#include <iterator>
using namespace std;

//usage: seq 10 | ./numbers

int main() {
    istream_iterator<int> in{cin};
    istream_iterator<int> eof;
    ostream_iterator<int> out{cout, " "};

    for (; in != eof; ++in, ++out) {
        const int n = *in;
        *out = n * n;
    }
    cout << endl;

    return 0;
}
