#include <iostream>
#include <cstdlib>

using namespace std;

int main() {
    srand(static_cast<unsigned int>(time(NULL)));

    auto secret = 1 + (rand() % 10);
    //cout << "secret: " << secret << endl;

    const int MAX_GUESSES = 5;
    int numGuesses = 1;
    do {
        cout << "Make a guess: ";
        int guess=-5;
        cin >> guess;
        cout << "** guess="<< guess<<endl;
        if (guess == secret) {
            cout << "Correct!\n";
            return 0;
        } else if (guess < secret) {
            cout << "Too low!\n";
        } else {
            cout << "Too high!\n";
        }
    } while (numGuesses++ < MAX_GUESSES);
    cout << "Tooooo many guesses\n";

    return 1;
}

