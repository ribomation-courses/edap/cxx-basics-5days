cmake_minimum_required(VERSION 3.10)
project(04_statements)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror -Wfatal-errors")

add_executable(guess-number guess-number.cxx)

