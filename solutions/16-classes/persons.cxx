#include <iostream>
#include <string>

using namespace std;
using namespace std::string_literals;


class Person {
    mutable string   name;
    unsigned age = 0;
public:
    static int instanceCount;

    Person(const string& name, unsigned age)
            : name{name}, age{age} {
        ++instanceCount;
    }

    ~Person() {
        --instanceCount;
    }

    Person() = default;

    const string& getName() const {
        return name;
    }

    void setName(const string& name) const {
        Person::name = name;
    }

    unsigned int getAge() const {
        return age;
    }

    void setAge(unsigned int age) {
        Person::age = age;
    }

    string toString() {
        return "Person{" + name + ", " + to_string(age) + "}";
    }

};

int Person::instanceCount = 0;

void doit(const Person& p) {
    p.setName("what ever");
    cout << "[doit] p.name = " << p.getName() << endl;
}


int main() {
    cout << "# persons: " << Person::instanceCount << endl;

    Person nisse{"Nisse"s, 42};
    cout << "nisse: " << nisse.toString() << endl;
    cout << "# persons: " << Person::instanceCount << endl;

    doit(nisse);

    {
        Person anna{"Anna"s, 32};
        cout << "anna: " << anna.toString() << endl;
        cout << "# persons: " << Person::instanceCount << endl;

        auto age = anna.getAge();
        cout << "anna.age = " << age << endl;
        anna.setAge(anna.getAge() + 1);
        cout << "anna.age = " << anna.getAge() << endl;
    }
    cout << "# persons: " << Person::instanceCount << endl;

    Person bosse;
    cout << "bosse: " << bosse.toString() << endl;


    return 0;
}
