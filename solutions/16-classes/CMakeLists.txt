cmake_minimum_required(VERSION 3.10)
project(16_classes)

set(CMAKE_CXX_STANDARD 17)

add_executable(persons persons.cxx)

