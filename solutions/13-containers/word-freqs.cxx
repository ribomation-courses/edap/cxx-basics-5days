#include <iostream>
#include <string>
#include <map>

using namespace std;

string strip(string s) {
    string result;
    for (auto ch : s) if (isalpha(ch)) result += ch;
    return result;
}

int main(int argc, char** argv) {
    map<string, unsigned> freqs;

    for (string word; cin >> word;) {
        freqs[strip(word)]++;
    }
    for (auto[word, freq] : freqs) {
        cout << word << ": " << freq << endl;
    }
    return 0;
}
