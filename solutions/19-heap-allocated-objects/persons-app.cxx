#include <iostream>
#include <vector>
#include <random>
#include "person.hxx"

int Person::instanceCount = 0;

using namespace std;
using namespace std::string_literals;

Person* create() {
    static vector<string>              names = {
            "Anna", "Berit", "Carin", "Diana", "Eva"
    };
    uniform_int_distribution<unsigned> nextIdx{0, static_cast<unsigned>(names.size() - 1)};
    uniform_int_distribution<unsigned> nextAge{20, 80};
    static random_device               r;

    return new Person{names[nextIdx(r)], nextAge(r)};
}

void func(Person p) {
    cout << "[func] p = " << p << endl;
}


int main() {
    unsigned        N = 10;
    vector<Person*> pers;

    for (auto k = 0U; k < N; ++k)
        pers.push_back(create());

    for (auto p: pers)
        cout << *p << endl;

    func(*pers[0]);

    for (auto p: pers)
        delete p;

    Person* p = create();
    delete p;

    cout << "------\n";
    {
        Person* theOneAndOnly = new Person{"Inge Vidare", 37};
        cout << "# persons = " << Person::count() << endl;

        vector<Person*> v;
        for (int k = 0; k < 3; ++k) v.push_back(theOneAndOnly);
        for (auto ptr: v) cout << *ptr << endl;
        //for (auto ptr: v) delete ptr;
        delete v[0];
    }

    cout << "# persons = " << Person::count() << endl;
    return 0;
}

