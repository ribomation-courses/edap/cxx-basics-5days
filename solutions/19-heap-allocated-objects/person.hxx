#pragma once

#include <iostream>
#include <string>

using namespace std;


class Person {
    string     name;
    unsigned   age;
    static int instanceCount;

public:
    Person(const string& name, unsigned int age = 42)
            : name{name}, age{age} {
        ++instanceCount;
        cout << "Person(string,unsigned) @ " << this << " [" << instanceCount << "]" << endl;
    }

    Person(const Person& that) : name{that.name}, age{that.age} {
        ++instanceCount;
        cout << "Person(const Person&) @ " << this << " [" << instanceCount << "]" << endl;
    }

    ~Person() {
        --instanceCount;
        cout << "~Person() @ " << this << " [" << instanceCount << "]" << endl;
    }

    static int count() { return instanceCount; }

    friend ostream& operator<<(ostream& os, const Person& p) {
        return os << "Person{name=" << p.name << ", age=" << p.age << "} @ " << &p;
    }
};



