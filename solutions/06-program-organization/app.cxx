#include <iostream>
#include <string>
#include "fibonacci.hxx"

using namespace std;


int main(int argc, char *argv[]) {
    //cout << "argv[0]=" << argv[0] << endl;
    const int arg = (argc == 1) ? 10 : stoi(argv[1]);
    cout << "fib(" << arg << ") = " << fibonacci(arg) << endl;
    return 0;
}

