#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::string_literals;

Person func(Person q) {
    cout << "[func] q = " << q << endl;
    return q;
}

int main() {
    Person p1{"Nisse"};
    cout << "[main] p1 = " << p1 << endl;

    Person p2 = func("Anna"s);
    cout << "[main] p2 = " << p2 << endl;

    Person p3 = func(p1);
    cout << "[main] p3 = " << p3 << endl;

    int* ptr = new int{42};
    delete ptr;

    return 0;
}

