#pragma once

#include <iostream>
#include <string>

using namespace std;


class Person {
    string   name;
    unsigned age;
public:
    Person(const string& name, unsigned int age = 42)
            : name{name}, age{age} {
        cout << "Person(string,unsigned) @ " << this << endl;
    }

    Person(const Person& that): name{that.name}, age{that.age} {
        cout << "Person(const Person&) @ " << this << endl;
    }

    ~Person() {
        cout << "~Person() @ " << this << endl;
    }

    friend ostream& operator<<(ostream& os, const Person& p) {
        return os << "Person{name=" << p.name << ", age=" << p.age << "} @ " << &p;
    }
};

