#include <iostream>

using namespace std;

int main() {
    cout << "Give amount: ";
    int a;
    cin >> a;

    cout << "Give rate: ";
    auto p = 0.0F; // float p=0;
    cin >> p;

    auto a1 = a * (1 + static_cast<double>(p) / 100);
    cout << "Final amount: " << a1 << endl;

    return 0;
}

