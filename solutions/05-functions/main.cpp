#include <iostream>

using namespace std;

int finalBalance(int a, float p) {
    return static_cast<int>(a * (1 + p / 100));
}

int initialBalance(int a, float p) {
    auto result = a / (1 + p / 100);
    cout << "** result=" << result << endl;
    return static_cast<int>(result + 0.5);
}

int main() {
    int amt = 1000;
    float rate = 1.25;
    cout << "amount=" << amt << ", rate=" << rate << "%\n";

    int amt1 = finalBalance(amt, rate);
    cout << "amount-1=" << amt1 << endl;

    int amt0 = initialBalance(amt1, rate);
    cout << "amount-0=" << amt0 << endl;

    return 0;
}