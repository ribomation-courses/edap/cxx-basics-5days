#pragma once

#include "Shape.hxx"

using namespace std;


class Rect : public Shape {
    using super = Shape;
    int width, height;

protected:
    Rect(const string& type, int width, int height)
            : super{type}, width{(width)}, height{height} {
        cout << type << "{" << width << ", " << height << "} @ " << this << endl;
    }

public:
    Rect(int width, int height) : Rect("rect", width, height) {}

    ~Rect() {
        cout << "~Rect() @ " << this << endl;
    }

    double area() const override {
        return width * height;
    }

    string toString() const override {
        ostringstream buf;
        buf << super::toString() << "{" << width << ", " << height << "}";
        return buf.str();
    }
};

