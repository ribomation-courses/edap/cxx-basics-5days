#pragma once

#include "Shape.hxx"

class Triangle : public Shape {
    using super = Shape;
    int base, height;

public:
    Triangle(int base, int height)
            : super{"triang"}, base{base}, height{height} {
        cout << "triang{" << base << ", " << height << "} @ " << this << endl;
    }

    ~Triangle() {
        cout << "~Triangle() @ " << this << endl;
    }

    double area() const override {
        return base * height / 2.0;
    }

    string toString() const override {
        ostringstream buf;
        buf << super::toString() << "{" << base << ", " << height << "}";
        return buf.str();
    }
};

