

#include <iostream>
#include "dummy.hxx"

using namespace std;

namespace ribomation {
    int glob = 1234;

    namespace foobar {
        int glob = 1234;

        Dummy::Dummy() {
            cout << "Dummy()" << endl;
        }


        int Dummy::magic() {
            return 42;
        }


        int func() {
            return 10;
        }


    }
}
