Home Work
====

Do this exercise after the course, during the time period until
next course in C++.

Objective
----

Write a C++ program that prints out a calendar for a given month.

    April 2018
    Mon Tue Wed Thu Fri Sat Sun
                             1
     2   3   4   5   6   7   8
     9  10  11  12  13  14  15
    16  17  18  19  20  21  22
    23  24  25  26  27  28  29
    30

### Command-line arguments:
* *No program argument*: Print calendar for current month
* *Month Name*: Print calendar for given month in current year
* *Month and year*: Print calendar for given month in given year

Write this program yourself, do not use an external library. However,
you might perhaps want to google around a little bit if you get stuck.

Extra
----
If you have some spare time, you can add options to
* Start week on Sunday (or any other week-day if you like)
* Print more than one month, if given an interval as input
* Set language, such as Swedish week-day and month names.
