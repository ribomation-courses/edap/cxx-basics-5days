C++ Basics, 5 days
====

Welcome to this course.
The syllabus can be found at
[cxx/cxx-basics](https://www.ribomation.se/cxx/cxx-basics.html)

Here you will find
* Installation instructions
* Support files
* Demo programs
* Solutions to the programming exercises
* [Home Work](./home-work.md)


Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a GIT clone operation

    git clone https://gitlab.com/ribomation-courses/edap/cxx-basics-5days.git
    cd cxx-basics-5days

Get the latest updates by a GIT pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have access to a C compiler running on preferably a Linux system, such as Ubuntu. Go for one of the listed solutions below.

* **Already have Linux installed on your laptop**<br/>
Then you are ready for the course. If it is not Ubuntu, then there might be some differences, but as long as you can handle it and do the translation yourself, there is no problem.
* **Already have access to a remote Linux system**<br/>
Same as above.
* **Is running Windows 10 on your laptop**<br/>
One of the biggest news of Windows 10, is that it now has native support for running Linux. This is called WSL (Windows Subsystem for Linux). You just have to enabled it. Follow the links below to proceed.
* **Otherwise**<br/>
You have to install VirtualBox and install Ubuntu into a VM. Follow the links below to proceed.

In addition, your need a C++ compiler such as GCC/G++ and some way to edit your program code, such as a decent text editor or a full blown IDE. Read more below for suggestions.

Installing Ubuntu @ VBox
----

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>

Installing WSL @ Windows 10
----

1. How to Install and Use the Linux Bash Shell on Windows 10<br/>
    <https://docs.microsoft.com/en-us/windows/wsl/install-win10/>
1. Once successfully installed WSL, you perhaps also want to have a handy right-click menu item of "Ubuntu Here"? Here is how to do it:
    <http://winaero.com/blog/add-bash-to-the-folder-context-menu-in-windows-10/>
1. If you would like to launch graphical applications from WSL, i.e. X-Windows apps, then you need to install a X server for Windows, such as VcXsrv, Cygwin-X or similar. <br/>
  * <https://sourceforge.net/projects/vcxsrv/>
  * <http://www.pcworld.com/article/3055403/windows/windows-10s-bash-shell-can-run-graphical-linux-applications-with-this-trick.html>
1. Once you have an X-Windows server running on Windows, you need to define the DISPLAY environment variable  in your ~/.bashrc file : `export DISPLAY=:0`

Installing GCC @ Ubuntu
----

Install the GCC C/C++ compiler in Ubuntu

    sudo apt-get install g++

N.B. the sudo command will prompt you for your Ubuntu login password

Suggestions for editor/ide
----

You also need to use a text editor or IDE to write your programs. If you already are familiar with tools like Emacs or Eclipse/C/C++, please go ahead and install them too. On the other hand, if you would like some advise I do recommend to choose one of these two suggestions:

* If you just want a decent text editor, then go for *Text Editor (gedit)*. It's already installed on Ubuntu and you can launch from the start menu in the upper left corner or from a terminal window with the command: <br/>
`gedit my-file.cxx &`

* If you want to use a really good IDE instead, then download and install the trial version of *JetBrains CLion*. This is my choice of C/C++ IDE and I will be using it during the course. CLion installed to Windows can be connected to a Ubuntu @ WSL. Read more about it below:
  * <https://www.jetbrains.com/clion/>
  * <https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html>


* Another, interesting IDE/smart-editor is *MS Visual Code*, which exists for Linux as well. Even VisualCode can be connected to WSL.
  * <https://code.visualstudio.com/>
  * <https://stackoverflow.com/questions/44450218/how-do-i-use-bash-on-ubuntu-on-windows-wsl-for-my-vs-code-terminal>

Installing a more Recent Version of GCC/G++
----

If you run an older version of Ubuntu, e.g. 16.04, it's default GCC version
is an old version of GCC. If you add another APT repository, you can
install the GCC version of your choice. Type these commands

    sudo add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install gcc-8 g++-8

After the install; compile with `g++-8` instead of `g++`, or create a symbolic link
from g++ to g++-8.

More information at
* [Ubuntu Toolchain test builds](https://launchpad.net/~ubuntu-toolchain-r/+archive/ubuntu/test?field.series_filter=trusty)
* [How to install gcc-7](https://askubuntu.com/questions/859256/how-to-install-gcc-7-or-clang-4-0)


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>
